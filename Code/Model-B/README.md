# Model B Simulator

## Introduction

Welcome to the Model B simulator! In the narrowest sense this is a place to see a particular stochastic partial differential equation solved that is of the following form:

$$ \frac{\partial \phi (x,t)}{\partial t} = M \nabla^2 \left(- W_o^2 \nabla^2 \phi + a_2 \phi + a_4 \phi^3 \right) + \xi (x,t)   $$

Where the driving force $\zeta(x,t)$ is a gaussian random variable with the following statistics:

$$ \left< \xi(x,t) \right> = 0, \,\,\,\,\,\,
\left< \xi(x_1, t_1) \xi(x_2, t_2) \right> = 2 M k_b T \, \nabla^2 \delta(x_2 - x_1) \delta(t_2 - t_1). $$

The importance of Model B or *Driven Cahn-Hilliard Equation* is that it is model of the dynamics of a conserved order parameter. The order parameter is conserved through the somewhat strange looking statistics of the random source term $\xi(x,t)$. 

### Conservative dynamics

Like the Kawasak kinetics implementation of the Ising model explained in the Model A simulator, model B implements kinetics for the order parameter $\phi$ such that its total value does not change with time. 

$$ \int_{\Omega} d^dx \,\, \phi (x,t)  = constant $$

Additionally, this means the order parameter must obey a continuity equation so that transport of the order parameter occurs in a local way. 

$$ \partial_t \phi + \nabla \cdot \mathbf{ J}_{\phi} = 0 $$

Thus, to add noise to our model, the noise must obey this conservation and must present itself as a random flux $\mathbf{J_\xi}$. Making use of the Onsagar reciprocity relationship for the order parameter. We can write the formal equation of motion for model B as follow:

$$ \frac{\partial \phi(x,t)}{\partial t} = - \nabla \cdot \left(M \nabla  \left\lbrace \frac{\delta F[\phi]}{\delta \phi} \right\rbrace + \mathbf{J}_\xi \right) $$

The laplacian  of the random flux leads to the formal yet unintuitive noise definition of $\xi$. In practice the noise is implemented as a flux in the field. 

Replacing the free energy with the usual $\phi^4$ theory we recover the equation of motion first mentioned. 

## Simulating

All of the code is written in *Julia* (0.3 release should work) but also requires *Python* and a python package called *Matplotlib*. 

- Julia info: http://julialang.org
- Matplotlib: http://matplotlib.org

To enable Matplotlib calls from Julia just install the PyPlot package inside the Julia shell via:

	julia> Pkg.add("PyPlots")
	
Afterwards the simulation can be from the command line from the installation folder

	> julia script.jl

Parameters can be changed inside the script.jl where the relevent parameters are commented.


## Contact Info

This model code repository and set of notes was made by me to learn and help teach topics in thermodynamics. The notes and code are free to use and distribute so long as they are properly attributed to me (eg. don't pretend you made these please).

- Nathan Smith
- nathan.smith5@mail.mcgill.ca
- http://www.nate-smith.net
