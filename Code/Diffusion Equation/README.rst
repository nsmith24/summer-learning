Diffusion Equation Codes
========================

Programming the diffusion equation will be our starting point for the summer.
The diffusion equation is a good starting point because we can solve it exactly
so we can check the accuracy of our results with the known solution. Also, once
you known how to build a code to integrate PDE's all other codes are basically
iterations on a theme.

For reference, the equation of motion is,

.. math::
    
    \frac{\partial n(x, t)}{\partial t} = D\nabla^2 n(x, t).

The exact solution is simpliest in Fourier space,

.. math::

    \tilde{n}(k, t) = e^{-D k^2}\tilde{n}(k, 0).

We'll use a new folder for each new algorithm so we can see a few different
examples of the same theory and try different programming techniques as well. 

