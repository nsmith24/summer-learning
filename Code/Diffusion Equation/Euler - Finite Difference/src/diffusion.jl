# Julia code to simulate the diffusion equation in 1 dimension

# State of the simulation
type State
    D   # Diffusion coefficient
    Δx  # Grid spacing
    Δt  # Time step size
    n   # Field
end


# Laplacian of the field
function ∇²(state) 
    n = state.n     # Field
    Δx = state.Δx   # Spacing
    N = length(n)   # Number of grid points in field

    # Make an array for output that is same type and size of field
    output = similar(n)

    # Inside points
    for i ∈ 2:N-1
        output[i] = (n[i-1] - 2 * n[i] + n[i + 1]) / Δx ^ 2
    end

    # Boundaries (periodic boundary conditions)
    output[1] = (n[N] - 2 * n[1] + n[2]) / Δx ^ 2
    output[N] = (n[N - 1] - 2 * n[N] + n[1]) / Δx ^ 2

    return output
end


function step!(state)
    # Note the '!' is a convention on julia that means the function
    # Changes it arguments in some way. Its "mutating"

    n = state.n
    Δt = state.Δt
    D = state.D

    n .= n .+  Δt * D * ∇²(state)
end
