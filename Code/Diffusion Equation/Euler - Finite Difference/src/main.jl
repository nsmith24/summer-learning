# Usage:
#   $ julia main.jl
 
# Include our little library of functions
include("diffusion.jl")

# Initialize our constants
N = 1000        # Length of array
Δx = 1.0        # Grid spacing
Δt = 0.1        # Time step size
D = 1.0         # Diffusion constant
n = zeros(N)    # Field

# Start array in step function shape
n[400:600] = 1.0

# Initialize the simulation state
state = State(D, Δx, Δt, n)

# write initial state to file
writedlm("initial.dat", state.n)

# Take 100 time steps
for t in 1:100
    step!(state)
end

# write final state to file
writedlm("final.dat", state.n)
