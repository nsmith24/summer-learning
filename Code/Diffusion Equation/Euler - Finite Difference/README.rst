Euler Time Stepping + Finite Difference
=======================================

Euler time stepping with finite difference is the simpliest place to start with
simulating the diffusion equation. Euler time stepping is the simpliest time
stepping algorithm in the sense that it is easy to implement and is fairly
intuitive to understand Basically, the field at :math:`t + \Delta t` is
calculated by Taylor expanding forword from the field at :math:`t`,

.. math::
    
    n(x, t + \Delta t) \approx n(x, t) + \frac{\partial n(x, t)}{\partial t}
    \Delta t.

The idea begin Euler time stepping and in general any *explicit* time stepping
algorithm is that the state in the future is written as an equation that only
depends on the state in the past. Usually this is acheived by some (potentially
high order) Taylor expansion.

An expression for the time derivative is supplied by the equation of motion,
which in this case is the diffusion equation. So our final expression for field
at :math:`t + \Delta t` is,

.. math::

    n(x, t + \Delta t) = \left(1 + \Delta t D \nabla^2 \right) n(x, t)

The only remaining equation to simulate this model is how to calculate the
spatial derivative. This is where we use finite differencing. Finite
differencing is inspired by the limit definition of the derivative,

.. math::
    
    \frac{d f(x)}{dx} = lim_{\Delta x \rightarrow 0} \frac{f(x + \Delta x) -
    f(x)}{\Delta x}.

If we simulate the field :math:`n(x)` on a fine enough grid then the grid
spacing :math:`\Delta x` is small enough that we've achieved this limit in some
sense so we can find the derivative at the grid point :math:`i` by
differencing,

.. math::
    
    \frac{d f(x_i)}{d x} \approx \frac{f(x_{i + 1}) - f(x_{i})}{\Delta x}

Analogous finite difference equations exist for higher order derivatives, as
well as more accurate difference equations for each order. For more details see
the wikipedia_ article

.. _wikipedia: https://en.wikipedia.org/wiki/Finite_difference 
