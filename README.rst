Nathan and Matt's Summer Learning Repository
============================================

Place to dump code, share papers and documentation and talk about things. On
bitbucket, files written in restructured text (:code:`.rst`) are rendered as
HTML and Latex equations are nicely typeset so we can use these files to
navigate the repository. 

Look in the :code:`/Code` folder for programming projects in the making and the
:code:`/Theory` folder for reading!

